# Entrega Tarea 12 Objetos JavaScript

## Ejercicio 1

### 1. Crear un metodo constructor llamada persona. Sus atributos son: nombre, edad y cedula. Construye los siguientes métodos para la clase:

### 1.1 mostrar(): Muestra los datos de la persona.
### 1.2 es_mayor_de_edad(): Devuelve un valor lógico indicando si es mayor de edad.

```javascript
class persona{
    constructor(nom,edad,id){
        this.nombre = nom;
        this.edad = edad;
        this.cedula = id:
    }
    mostrar(){
        console.log("Datos personales: ");
        console.log(`Nombre: ${this.nombre} Edad: ${this.edad} Cedula: ${this.cedula}`);
    }
    esMayor(){
        if(this.edad >= 18){
            return true;
        }
        if(this.edad >= 0 && this.edad < 18){
            return false;
        }
        if(this.edad < 0){
            return "ingrese una edad valida";
        }
    }
}

let nombre = prompt("ingrese el nombre");
let edad = parseInt(prompt("ingrese la edad"));
let cedula = prompt("ingrese la cedula")
let persona1 = new persona(nombre, edad, cedula);
persona1.mostrar();
console.log(persona1.esMayor());
```

## Ejercicio 2

### Crea un metodo constructor llamado cuenta que tendrá los siguientes atributos: titular (que es nombre de la persona) y cantidad. El titular será obligatorio y la cantidad es opcional. Construye los siguientes métodos para el metodo:
### 2.1 mostrar(): Muestra los datos de la cuenta.
### 2.2 ingresar(cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa, no se hará nada.
### 2.3 retirar(cantidad): se retira una cantidad a la cuenta. La cuenta puede estar en números rojos.

```javascript
class cuenta{
    constructor(titular,saldo){
        this.titular = titular;
        this.cantidad = saldo;
    }
    mostrar(){
        console.log(`Titular: ${this.titular} Saldo: ${this.cantidad}`);
    }
    ingresar(valor){
        if(valor > 0){
            this.cantidad += valor; // this.cantidad = this.cantidad + valor
            console.log("Consignación exitosa");
        }
        if(valor <= 0){
            console.log("No se pudo procesar tu solicitud.");
            console.log("El valor ingresado no es valido.");
        }
    }
    retirar(valor){
        if(valor <= this.cantidad){
            this.cantidad -= valor;
            console.log("Retiro exitoso");
            console.log(`Tu saldo actual es : ${this.cantidad}`);
        }
        if(valor > this.cantidad){
            console.log("Saldo insuficiente");
            console.log(`Tu saldo es de : ${this.cantidad}`);
        }
    }

}
let cuentaPersona1 = ["Pepio",100000];
let cuenta1 = new cuenta(cuentaPersona1[0],cuentaPersona1[1]);
cuenta1.ingresar(50000);
cuenta1.retirar(45000);
cuenta1.mostrar();
```

## Ejercicio 3

### Crear un metodo constructor llamado formulas. Construye los siguiente metodos para la clase:
### 3.1 sumar(entero, entero)
### 3.2 fibonacci(cantidad) a partir de una entero sacar los numeros
### 3.3 operacion_modulo(cantidad) a partir de una cantidad mostrar cuales dan residuo 0
### 3.4 primos(cantidad) a partir de una cantidad mostrar cuales son numeros primos.

```javascript
class formulas{
    sumar(num1,num2){
        return num1+num2;
    }
    
    fibonaci(cantidad){
        let fibo =[0,1]
        for(i = 2; i<=cantidad; i++){
            fibo.push(fibo[i-1]+fibo[i-2]);
            console.log(fibo[i]);
        }
    }

    modulo(cantidad){
        for(i = 1;i <= cantidad;i++){
            if(i % 2 == 0){
                console.log(i);
            }
        }
    }

    primos(cantidad){
        let validador = false;
        for(let i = 0; i < cantidad;i++){
            for(let x = 2; x < i ;x++){
                if(i % x === 0){
                    validador = false;
                }
                else{
                    validador = true;                    
                }
            }
            if(validador){
                console.log(`El número ${i} es primo`);
            }
            else{
                console.log(`El número ${i} no es primo`);
            }
        }
    }
}


let prueba = new formulas();
console.log(prueba.sumar(2,4));
prueba.primos(23);
```

## Ejercicio 4

### Crear un metodo constructor llamado persona. Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura Construye los siguiente metodos para la clase:
### 4.1 calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)), si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
### 4.2 esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano.
### 4.3 comprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.
```javascript
class persona{
    constructor(nombre,edad,dni,sexo,peso,altura){
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }
    calcularIMC(){
        this.imc =  (this.peso/(this.altura * this.altura))
        if(this.imc < 20){
            return -1;
        }
        if(this.imc >= 20 && this.imc <= 25){
            return 0;
        }
        if(this.imc > 25){
            return 1;
        }

    }
    esMayorDeEdad(){
        if(this.edad >= 18){
            return true;
        }
        else{
            return false;
        }
    }
    comprobarSexo(){
        if(this.sexo == "H" || this.sexo != "M"){
            this.sexo = "H";
            console.log("Es hombre");
        }
        else{
            console.log("Es mujer");;
        }
    }
}

let person1 = new persona("Andrés",31,"1053808595","H",80,186);
console.log(person1.esMayorDeEdad());
person1.comprobarSexo();
```
## Ejercicio 5

### Crear un metodo constructor llamado contraseña. Sus atributos longitud y contraseña Construye los siguiente metodos para la clase:

### 5.1 esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números.
### 5.2 generarPassword():  genera la contraseña del objeto con la longitud que tenga.
### 5.3 seguridadPaswword(); indicar si la contraseña es debil contiene entre 1 a 6 caracteres (caracteres numeros y letras), media (7 a 10 caracteres numeros y letras) o fuerte (11 a 20 caracteres letras y caracteres especiales)

```javascript
class contraseña{
    constructor(longitud,contraseña){        
        this.longitud = longitud;
        this.contraseña = contraseña;
    }

    esFuerte(){
        if(this.contraseña.length > 5){
            return "Es fuerte";
        }
    }

    generarPassword(tamaño){
        let chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";       
        let contraseñaGenerada = '';
        for (let i=0; i<tamaño; i++){
            let rnum = Math.floor(Math.random() * chars.length);
            contraseñaGenerada += chars.substring(rnum,rnum+1);
        }
        return contraseñaGenerada;
    }
}
```

## Ejercicio_6.js  
6. Implementar un objeto que modele un contador. Un contador se puede incrementar o decrementar, recordando el valor actual. Al resetear un contador, se pone en cero. Además es posible indicar directamente cual es el valor actual. Este objeto debe entender los siguientes mensajes:   
6.1 reset()   
6.2 inc()  
6.3 dec()  
6.4 valorActual()   
6.5 valorActual(nuevoValor)  
P.ej. si se evalúa la siguiente secuencia contador.valorActual(10) contador.inc() contador.inc() contador.dec() contador.inc() contador.valorActual() el resultado debe ser 12.
```javascript
class contador{
    constructor(){
        this.conteo = 0;
        this.ultimaAccion = "";
    }
    reset(){
        this.conteo = 0;
        this.ultimaAccion = "reset";   
    }
    inc(){
        this.conteo += 1;
        this.ultimaAccion = "incremento";
    }
    dec(){
        this.conteo -= 1;
        this.ultimaAccion = "decremento";
    }
    valorActual(valorNuevo){
        if (valorNuevo != null){
            this.conteo = valorNuevo;
            this.ultimaAccion = "actualización"
        }
    }
    if (valorNuevo = null){
        console.log(`valor actual: ${this.conteo}`)
    }
      ultimoComando(){
        console.log(`El último comando fue ${this.ultimaAccion}`)
    }
    
}
let contadorNuevo = new contador();
contadorNuevo.valorActual(10)
contadorNuevo.inc()
contadorNuevo.inc()
contadorNuevo.dec()
contadorNuevo.inc()
contadorNuevo.valorActual()
```
## Ejercicio_7.js
7. Agregar al contador del ejercicio 6, la capacidad de recordar un String que representa el último comando que se le dio. Los Strings posibles son "reset", "incremento", "decremento" o "actualizacion" (para el caso de que se invoque valorActual con un parámetro). Para saber el último comando, se le envía al contador el mensaje ultimoComando().
En el ejemplo del ejercicio 3, si luego de la secuencia indicada se evalúa contador.ultimoComando() el resultado debe ser "incremento"
```javascript
class contador{
    constructor(){
        this.conteo = 0;
        this.ultimaAccion = "";
    }
    reset(){
        this.conteo = 0;
        this.ultimaAccion = "reset";   
    }
    inc(){
        this.conteo += 1;
        this.ultimaAccion = "incremento";
    }
    dec(){
        this.conteo -= 1;
        this.ultimaAccion = "decremento";
    }
    valorActual(valorNuevo){
        if (valorNuevo != null){
            this.conteo = valorNuevo;
            this.ultimaAccion = "actualización"
        }
    }
    if (valorNuevo = null){
        console.log(`valor actual: ${this.conteo}`)
    }
      ultimoComando(){
        console.log(`El último comando fue ${this.ultimaAccion}`)
    }
    
}
let contadorNuevo = new contador();
contadorNuevo.valorActual(10)
contadorNuevo.inc()
contadorNuevo.inc()
contadorNuevo.dec()
contadorNuevo.inc()
contadorNuevo.valorActual()
```
## Ejercicio_8.js
8. Implementar un objeto que modele a Chimuela, una dragona de la que nos interesa saber qué energía tiene en cada momento, medida en joules. En el metodo constructor simplificado que nos piden implementar, las únicas acciones que vamos a contemplar son: cuando Chimuela come una cantidad de comida especificada en gramos, en este caso adquiere 4 joules por cada gramo, cuando Chimuela vuela una cantidad de kilómetros, en este caso gasta un joule por cada kilómetro, más 10 joules de costo fijo de despegue y aterrizaje. La energía de Chimuela nace en 0. El objeto que implementa este metodo constructor de Chimuela, debe entender los siguientes mensajes: 
8.1 comer(gramos) 
8.2 volar(kilometros) 
8.3 energia() P.ej. si sobre un REPL(Read-Eval-Print-Loop)(Lectura-Evaluación-Impresión) recién lanzado se evalúa la siguiente secuencia Chimuela.comer(100) Chimuela.volar(10) Chimuela.volar(20) Chimuela.energia() el resultado debe ser 350.
```javascript
class chimuela
{
    constructor()
    {
        this.energia = 0
    }
    comer(cantidad)
    {
        this.energia += (cantidad*4)

    }
    volar(distancia)
    {
        this.energia -=(distancia+10)
    }
    VerEnergia()
    {
        console.log(`Energía actual: ${this.energia}`)
    }

}
let chimuela1 = new chimuela()
chimuela1.comer(100)
chimuela1.volar(10)
chimuela1.volar(20)
chimuela1.VerEnergia()
```
## Ejercicio_9.js
9. Agregar al metodo constructor de Chimuela del ejercicio 8, la capacidad de entender estos mensajes: estaDebil(), Chimuela está débil si su energía es menos de 50. estaFeliz(), Chimuela está feliz si su energía está entre 500 y 1000. cuantoQuiereVolar(), que es el resultado de la siguiente cuenta. De base, quiere volar (energía / 5) kilómetros, p.ej., si tiene 120 de energía, quiere volar 24 kilómetros. Si la energía está entre 300 y 400, entonces hay que sumar 10 a este valor, y si es múltiplo de 20, otros 15. Entonces, si Chimuela tiene 340 de energía, quiere volar 68 + 10 + 15 = 93 kilómetros. Para probar esto, sobre un REPL recién lanzado darle de comer 85 a Chimuela, así la energía queda  10. Para saber si n es múltiplo de 20 hacer: n % 20 == 0. Probarlo en el REPL(Read-Eval-Print-Loop)(Lectura-Evaluación-Impresión)
```javascript
class chimuela
{
    constructor()
    {
        this.energia = 0
    }
    comer(cantidad)
    {
        this.energia += (cantidad*4)

    }
    volar(distancia)
    {
        this.energia -=(distancia+10)
    }
    VerEnergia()
    {
        console.log(`Energía actual: ${this.energia}`)
    }
    estaDebil()
    {
        if(this.energia < 50)
            return true
        else
        {
            return false
        }
    }
    estafeliz()
    {
        if(this.energia >= 500 && this.energia <= 1000)
        {
            return true
        }
        else
        {
            return false
        }
    }
    cuandoQuiereVolar()
    {
        let base =this.energia/5
        let total = 0
        let plus= 0
        if(this.energia >= 300 && this.energia <= 400)
        {
            plus += 10
        }
        if(this.energia % 20 == 0)
        {
            plus += 15
        }
        total = base + plus
        console.log(`Quiere volar ${total} kilometros`)
    }
}
let chimuela1 = new chimuela()
chimuela1.comer(100)
chimuela1.volar(10)
chimuela1.volar(20)
chimuela1.VerEnergia()
chimuela1.estaDebil()
chimuela1.estafeliz()
chimuela1.cuandoQuiereVolar()
```
## Ejercicio_10.js
10. Implementar un objeto que represente una calculadora sencilla, que permita sumar, restar y multiplicar. Este objeto debe entender los siguientes mensajes: 10.1 cargar(numero) 10.2 sumar(numero) 10.3 restar(numero) 10.4 multiplicar(numero) 10.5 valorActual() P.ej. si se evalúa la siguiente secuencia calculadora.cargar(0) calculadora.sumar(4) calculadora.multiplicar(5) calculadora.restar(8) calculadora.multiplicar(2) calculadora.valorActual() el resultado debe ser 24.
```javascript
class calculadora
{
    constructor()
    {
        this.total = 0
    }
    cargar(numero)
    {
        this.total = numero
    }
    sumar(numero)
    {
        this.total += numero
    }
    restar(numero)
    {
        this.total -= numero
    }
    multiplicar(numero)
    {
        this.total *= numero
    }
    valorActual()
    {
        console.log(`Valor actual: ${this.total}`)
    }
}

let calc = new calculadora()
calc.cargar(0)
calc.sumar(4)
calc.multiplicar(5)
calc.restar(8)
calc.multiplicar(2)
calc.valorActual()
```
## Ejercicio_11.js
11. Crear un metodo constructor llamado Libro. Sus atributos título del libro, autor, número de ejemplares del libro y número de ejemplares prestados los siguiente metodos para la clase:  

préstamo() que incremente el atributo correspondiente cada vez que se realice un préstamo del libro. No se podrán prestar libros de los que no queden ejemplares disponibles para prestar. Devuelve true si se ha podido realizar la operación y false en caso contrario.   
devolucion() que decremente el atributo correspondiente cuando se produzca la devolución de un libro. No se podrán devolver libros que no se hayan prestado. Devuelve true si se ha podido realizar la operación y false en caso contrario.   
toString() para mostrar los datos de los libros.
```javascript
class libro
{
    constructor(titulo,autor,nEjemplares,nEjemplaresPrestados)
    {
        this.titulo = titulo
        this.autor = autor
        this.nEjemplares=nEjemplares
        this.nEjemplaresPrestados = nEjemplaresPrestados
    }
    prestar()
    {
        if(this.nEjemplares > 0)
        {
            this.nEjemplaresPrestados += 1
            this.nEjemplares -= 1
            return true
        }
        else
        {
            return false
        }
    }
    devolver()
    {
        if(this.nEjemplaresPrestados > 0)
        {
            this.nEjemplaresPrestados -= 1
            this.nEjemplares += 1
            return true
        }
        else
        {
            return false
        }
    }
    toString()
    {
        console.log(`Titulo: ${this.titulo}`)
        console.log(`Autor: ${this.autor}`)
        console.log(`Ejemplares disponibles: ${ this.nEjemplares}`)
        console.log(`Ejemplares Prestados: ${this.nEjemplaresPrestados}`)
    }
}
```
## Ejercicio_12.
12. Se está pensando en el diseño de un juego que incluye la nave espacial Enterprise. En el juego, esta nave tiene un nivel de potencia de 0 a 100, y un nivel de coraza de 0 a 20. La Enterprise puede encontrarse con una pila atómica, en cuyo caso su potencia aumenta en 25. encontrarse con un escudo, en cuyo caso su nivel de coraza aumenta en 10. recibir un ataque, en este caso se especifican los puntos de fuerza del ataque recibido. La Enterprise �para� el ataque con la coraza, y si la coraza no alcanza, el resto se descuenta de la potencia. P.ej. si la Enterprise con 80 de potencia y 12 de coraza recibe un ataque de 20 puntos de fuerza, puede parar solamente 12 con la coraza, los otros 8 se descuentan de la potencia. La nave debe quedar con 72 de potencia y 0 de coraza. Si la Enterprise no tiene nada de coraza al momento de recibir el ataque, entonces todos los puntos de fuerza del ataque se descuentan de la potencia. La potencia y la coraza tienen que mantenerse en los rangos indicados, p.ej. si la Enterprise tien 16 puntos de coraza y se encuentra con un escudo, entonces queda en 20 puntos de coraza, no en 26. Tampoco puede quedar negativa la potencia, a lo sumo queda en 0. La Enterprise nace con 50 de potencia y 5 de coraza. Implementar este metodo constructor de la Enterprise, que tiene que entender los siguientes mensajes:   
12.1 potencia()   
12.2 coraza()   
12.3 encontrarPilaAtomica()   
12.4 encontrarEscudo()  
 12.5 recibirAtaque(puntos)  
P.ej. sobre un REPL recién lanzado, después de esta secuencia enterprise.encontrarPilaAtomica() enterprise.recibirAtaque(14) enterprise.encontrarEscudo() la potencia de la Enterprise debe ser 66, y su coraza debe ser 10.
```javascript
class enterprise
{
    constructor(potencia, coraza)
    {
        this.potencia = 50
        this.coraza = 5
    }
    encontrarPilaAtomica()
    {
        if(this.potencia >= 100)
        {
            console.log("Potencia Maxima")
        }
        else
        {
            this.potencia +=  25
        }
        if(this.potencia > 100)
        {
            this.potencia = 100
        }
        console.log(`La potencia actual es de ${this.potencia}`)
    }

    encontrarEscudo()
    {
        if(this.coraza >= 20)
        {
            console.log("Escudo Maximo")
        }
        else
        {
            this.coraza +=  10
        }
        if(this.coraza > 20)
        {
            this.coraza = 20
        }
        console.log(`La coraza actual es de ${this.coraza}`)
    }

    recibirAtaque(puntosDaño)
    {
        let total = this.coraza - puntosDaño
        if(total >= 0)
        {
            this.coraza = total
        }
        if(total < 0)
        {
            this.coraza = 0
            this.potencia -= (total * -1)
        }
    }
    mostrarStats()
    {
        console.log("los stats actuales son: ")
        console.log(`La coraza actua es de:  ${this.coraza}`)
        console.log(`La potencia actual es de:  ${this.potencia}`)
    }

}

let nave1 = new enterprise()
nave1.encontrarPilaAtomica()
nave1.recibirAtaque(14)
nave1.encontrarEscudo()
nave1.mostrarStats()
```
## Ejercicio_13.js
13. Agregar al metodo constructor de la Enterprise del ejercicio 12, la capacidad de entender estos mensajes. fortalezaDefensiva(), que es el máximo nivel de ataque que puede resistir, o sea, coraza más potencia. necesitaFortalecerse(), tiene que ser true si su coraza es 0 y su potencia es menos de 20. fortalezaOfensiva(), que corresponde a cuántos puntos de fuerza tendría un ataque de la Enterprise. Se calcula así: si tiene menos de 20 puntos de potencia entonces es 0, si no es (puntos de potencia - 20) / 2.
```javascript
class enterprise
{
    constructor(potencia, coraza)
    {
        this.potencia = 50
        this.coraza = 5
    }
    encontrarPilaAtomica()
    {
        if(this.potencia >= 100)
        {
            console.log("Potencia Maxima")
        }
        else
        {
            this.potencia +=  25
        }
        if(this.potencia > 100)
        {
            this.potencia = 100
        }
        console.log(`La potencia actual es de ${this.potencia}`)
    }

    encontrarEscudo()
    {
        if(this.coraza >= 20)
        {
            console.log("Escudo Maximo")
        }
        else
        {
            this.coraza +=  10
        }
        if(this.coraza > 20)
        {
            this.coraza = 20
        }
        console.log(`La coraza actual es de ${this.coraza}`)
    }

    recibirAtaque(puntosDaño)
    {
        let total = this.coraza - puntosDaño
        if(total >= 0)
        {
            this.coraza = total
        }
        if(total < 0)
        {
            this.coraza = 0
            this.potencia -= (total * -1)
        }
    }
    mostrarStats()
    {
        console.log("los stats actuales son: ")
        console.log(`La coraza actua es de ${this.coraza}`)
        console.log(`La potencia actual es e ${this.potencia}`)
    }
    fortalezaDefensiva()
    {
        return this.coraza + this.potencia
    }
    necesitaFortalecer()
    {
        if(this.potencia <= 20 && this.coraza == 0 )
        {
            return true
        }
        else
        {
            return false
        }
    }
    fortalezaOfensiva()
    {
        if(this.potencia < 20)
        {
            return 0
        }
        if(this.potencia > 20)
        {
            return (this.potencia-20)/2
        }
    }
}
```
## Ejercicio_14.js
14. Un taller de diseño de autos quiere estudiar un nuevo prototipo. Para eso, nos piden hacer un metodo constructor concentrado en las características del motor. El prototipo de motor tiene 5 cambios (de primera a quinta), y soporta hasta 5000 RPM. La velocidad del auto se calcula así: (rpm / 100) * (0.5 + (cambio / 2)). P.ej. en tercera a 2000 rpm, la velocidad es 20 * (0.5 + 1.5) = 40. También nos interesa controlar el consumo. Se parte de una base de 0.05 litros por kilómetro. A este valor se le aplican los siguientes ajustes: Si el motor está a más de 3000 rpm, entonces se multiplica por (rpm - 2500) / 500. P.ej., a 3500 rpm hay que multiplicar por 2, a 4000 rpm por 3, etc. Si el motor está en primera, entonces se multiplica por 3. Si el motor está en segunda, entonces se multiplica por 2. Los efectos por revoluciones y por cambio se acumulan. P.ej. si el motor está en primera y a 5000 rpm, entonces el consumo es 0.05 * 5 * 3 = 0.75 litros/km. El metodo constructor debe entender estos mensajes: arrancar(), se pone en primera con 500 rpm. subirCambio() bajarCambio() subirRPM(cuantos) bajarRPM(cuantos) velocidad() consumoActualPorKm()
```javascript
class motor
{
    constructor()
    {
        this.cambioActual = 0;
        this.rpm = 0;
        this.velocidad = 0
        this.consumo = 0
    }
    arrancar()
    {
        this.cambioActual = 1
        this.rpm = 500
    }
    subirCambio()
    {
        this.cambioActual += 1;
    }
    bajarCambio()
    {
        this.cambioActual -= 1;
    }
    subirRPM(cantidad)
    {
        this.rpm += cantidad
    }
    bajarRPM(cantidad)
    {
        this.rpm -= cantidad
    }
    velocidadTotal()
    {
        this.velocidad = (this.rpm/100)*(0.5+(this.cambioActual/2))
    }
    consumoActualPorKm()
    {
        this.consumo = 0.5
        if(this.cambioActual == 1)
        {
            this.consumo *= 3;
        }
        if(this.cambioActual == 2)
        {
            this.consumo *= 2
        }
        if(this.rpm > 3000)
        {
            let calculo = (this.rpm-2500)/500
            this.consumo = this.consumo * calculo
        }
        return this.consumo
    }
}
```
